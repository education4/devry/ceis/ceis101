//LED pins
int ledArray[] {
  13, //red
  12, //yellow
  11, //green
};
//end LEDs

//sound pins
int buzzerPin = 9; //passive buzzer

// notes
unsigned int notes[] {
  784, // G
  880, // A
  932, // Bb
};

int song[][4] = {
  { // notes
    2,
    1,
    2,
    0,
  },

  { // duration in tenths of a second
    10,
    5,
    5,
    10,
  }
};

int songLength;

void setup() {
  for (int pin : ledArray) {
    pinMode(pin, OUTPUT);
  }
  pinMode(buzzerPin, OUTPUT);

  songLength = sizeof(song[0]) / sizeof(song[0][0]);
}

// the loop function runs over and over again forever
void loop() {
  for (int i = 0; i < songLength; ++i) {
    tone(buzzerPin, notes[song[0][i]]);
    digitalWrite(ledArray[song[0][i]], HIGH);
    delay(song[1][i] * 100);
    digitalWrite(ledArray[song[0][i]], LOW);
  }
}
