int buzzerPin = 8;     // choose the pin for the buzzer
int inputPin = 7;   // choose the input pin (for PIR sensor)
int pirState = LOW; // we start, assuming no motion detected
int val = 0;        // variable for reading the pin status
   
 void setup() {
  pinMode(buzzerPin, OUTPUT);      // declare buzzer as output
  pinMode(inputPin, INPUT);     // declare sensor as input
 }
  
 void loop(){
   val = digitalRead(inputPin);    // read input value
   if (val == HIGH) {              // check if the input is HIGH
      digitalWrite(buzzerPin, HIGH);  // turn buzzer ON  
   }
   else {
      digitalWrite(buzzerPin, LOW); // turn buzzer OFF
   }
 }
