#include <WiFi.h>

void alarmOn();
void alarmOff();
void connectWiFi();
void recordEvent();

const int ledPin = 27,
          buzzerPin = 33,
          pirStatePin = 13;
const int alarmPins[]{ledPin,
                      buzzerPin
};

int
  pirState, 
  pirStatePrime = LOW;

const char
  * ssid = "belkin.5d2",
  * password = "6e96f6b9";

void setup(){
  for (int alarmPin : alarmPins){
    pinMode( alarmPin, OUTPUT);
  }
  pinMode( pirStatePin, INPUT);
  
  Serial.begin(115200);
  pinMode(ledPin, OUTPUT);

  delay(10);

}

void loop(){
  pirState = digitalRead(pirStatePin);
  
  if (pirState && !pirStatePrime){
    alarmOn();
  }
  else if (pirStatePrime && !pirState){
    alarmOff();
  }
}

void alarmOn(){
  for (int alarmPin : alarmPins){
      digitalWrite( alarmPin, HIGH);
    }
    recordEvent();
    
    pirStatePrime = HIGH;

    delay(1000);
}

void alarmOff(){
  for (int alarmPin : alarmPins){
      digitalWrite( alarmPin, LOW);
    }
    
    pirStatePrime = LOW;
}

void connectWiFi(){
  Serial.println("Connecting to " + String(ssid) + '\n');

  int status = WiFi.begin(ssid, password);

  while (status != WL_CONNECTED){
    Serial.println(WiFi.status());
    delay(500);
    Serial.println('.');
    status = WiFi.begin(ssid, password);
  }
  
  Serial.println();
  Serial.println("WiFi connected.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void recordEvent(){
  static const char
    * server = "maker.ifttt.com",
    * resource = "/trigger/motion_sensed/with/key/fbiWn2XLs7CbpP7dVAqP7reNOBnZp_mZllRaKJjawb9"
    ;

  connectWiFi();
    
  Serial.print("Connecting to " + String(server));

  WiFiClient client;
  int retries = 5;

  while(!client.connect(server, 80) && (retries-- > 0)){
    Serial.print('.');
  }

  Serial.println();
  if(!client.connected()){
    Serial.println("Failed to connect");
    Serial.println();
    WiFi.disconnect();
    return;
  }

  client.println(String("POST ") + String(resource) + " HTTP/1.1");
  client.println(String("Host: ") + String(server)); 
  client.println("Connection: close\r\n");
  int timeout = 5 * 10; // 5 seconds             
  while(!client.available() && (timeout-- > 0)){
    delay(100);
  }
  if(!client.available()) {
    Serial.println("No response...");
  }
  while(client.available()){
    Serial.write(client.read());
  }
  Serial.println();  
  Serial.println("Closing connection");
  Serial.println();
  client.stop(); 

  WiFi.disconnect();
}
