# Week6.ino
Week6.ino alerts, i.e. powers buzzer and LED, when the PIR motion detector senes motion. The event is then logged with a time stamp by an IFTTT applet in a google sheet.

# Week6.notino
Week6.notino is the code provided in the assignment guide (modified to be more readily understood) which allows a user to control the state of an LED remotely. It also buzzez when the PIR motion detector senses motion.
