const int //a is for analog-in pins
  buzzerPin = 13,
  aBuzzerAnodePin = 0,
  aBuzzerCathodePin = 1,
  pirStatePin = 7;

int
  pirStateInitial = LOW,
  pirState = LOW,
  count = 0;

void setup(){
  pinMode(buzzerPin, OUTPUT);
  pinMode(pirStatePin, INPUT);

  Serial.begin(9600);

  Serial.println("Code written by Villalpando, Adrian \n");
}

void loop(){
  pirState = digitalRead(pirStatePin);
  digitalWrite(buzzerPin, pirState);

  if(pirState == HIGH){
    if(pirStateInitial == LOW){
      static float 
        buzzerAnodeVoltage,
        buzzerCathodeVoltage;
      buzzerAnodeVoltage = analogRead(aBuzzerAnodePin);
      buzzerCathodeVoltage = analogRead(aBuzzerCathodePin);
      buzzerAnodeVoltage = buzzerAnodeVoltage*5.0 / 1023;
      buzzerCathodeVoltage = buzzerCathodeVoltage*5.0 / 1023;
      
      ++count;
      Serial.println("Motion detected!");
      Serial.println("Count: " + String(count));
      Serial.println("Av: " + String(buzzerAnodeVoltage) + '\t' +
                     "Cv: " + String(buzzerCathodeVoltage));
      Serial.println();

      pirStateInitial = HIGH;
    }
  }
  else{
    if(pirStateInitial == HIGH){
      Serial.println("Motion ended!\n");

      pirStateInitial = LOW;
    }
  }
}
