# CEIS 101

Introductory level course introducing IT terminology and buzzwords, embedded systems (Arduino), and career opportunities. Course work available consists of putting together a rudimentary alarm system (i.e. a bread board containing a motion sensor, an LED, an active buzzer, and an ESP32 microcontroller). All code for this project was provided by DeVry, though I at least refactored it to increase legibility. Any differences from the assigned task and provided code will be noted below.

## Week 1
Ensure all necessary components were purchased and available.

## Week 2
Create an exteremely rudimentary flowchart and IPO for the project.

## Week 3
Set up a blinking LED.  
Created and coded a circuit that attempts to play a looped bar of 'Shchedryk' and lights up a corresponding LED for each note.

## Week 4
Set up a circuit that buzzes when the PIR motion sensor detects motion.

## Week 5
Use the serial monitor to track differences in voltage between circuits comprised of variable resistors and an active buzzer.

## Week 6
Set up a circuit using an ESP32 microcontroller that buzzes when the PIR motion sensor detects motion and has a remotely controllable LED.  
Created and coded a circuit that alerts when motion is detected and logs the event in a Google Sheet using an IFTTT applet

## Week 7
Compile the prior weeks work into a single presentation, hosted on a Wix website (not available).